start
```
vagrant up
```

stop
```
vagrant halt
```

destroy
```
vagrant destroy -f
```
ssh
```
vagrant ssh [machine_name]
vagrant ssh master
vagrant ssh node01
vagrant ssh node02
```
